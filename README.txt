Bug Reports or Questions: alicef763@gmail.com

If you don't want to build for Android and instead want to build
for generic web browser(IE/Chrome/FF/Safari) on desktop or
mobile as a standard webapp, only download the 
/assets/www/ folder. :-)

COMPILING:
Android or Webapp:
	
	Java is required for the normal build process.

	The files you should edit to create your game are
	game.js - Put your game logic here.
	data.js - Put your games data(assets) here.
	
	The actual art resource files(png, whatever) go in
	/www/img/

	When you are ready to build/test,
	run /www/js/compile-{target platform}.cmd.
	So for Android, run compile-android.cmd.
	For Webapp, run compile-desktop.cmd.

Webapp:

	No further steps are required,
	just publish the /www/ folder somewhere.

Android only:
	
	AIDE/PhoneGap running on an Android device is required
	for the normal build process.

	Use the GIT Clone feature of AIDE/PhoneGap to import your
	project from whatever GIT repo you have it stored in.

	Make sure the project is open in AIDE/PhoneGap after
	you GIT Clone.
	Press the Menu button on the Android device while in
	AIDE/PhoneGap and choose Run.
	AIDE/PhoneGap will run it's build process and generate
	a .apk file, which is an Android program binary file.

	The normal location that AIDE/PhoneGap places the
	generated .apk is /sdcard/AppProjects/{name of project}/bin/

	The steps for publishing an app onto Google Play are
	beyond the scope of this document, but AIDE/PhoneGap
	can cater to that need.