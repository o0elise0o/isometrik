isometrik.game = (function () {
	return {
		// A game specific variable used as a toggle to ensure that
		//    the tile map drawing in isometrik.game.loop() only happens once.
		stop: false,
		main: function ()
		{
			isometrik.ajax.request("./js/level1.js", isometrik.loadLevelData);
			isometrik.world.initialize(64, 64);
			isometrik.hideLoadingAnimation();
			isometrik.startLoop(50, true);
		},
		loop: function ()
		{
			if (isometrik.running == true)
			{
				//isometrik.game.XY = [Math.floor(Math.random()*parseInt(isometrik.container.style.width,10)+0),Math.floor(Math.random()*parseInt(isometrik.container.style.height,10)+0),0];
				//
				//isometrik.draw.image.tile(Math.floor(Math.random()*8+0), isometrik.game.XY, "ground");
				//isometrik.draw.text(isometrik.game.XY, isometrik.game.XY[0], isometrik.game.XY[1])
				//
				//isometrik.game.oldXY = isometrik.game.XY;
				if (isometrik.game.stop == false)
				{
					//isometrik.draw.image.map(isometrik.data.maps[0],[0,0,0]);
					// for (a=0;a<=31;a++) 
					// {
					// 	isometrik.draw.isometric.tile(isometrik.create.solid("?",32,32),[a * 64,0,1],0);
					// }
					isometrik.draw.isometric.squareMap(isometrik.data.testIsoMap,[0,0 - isometrik.tileSizeY,0],0);
					console.log(isometrik.convert.imageToArray(isometrik.data.tiles[1]));
					//isometrik.draw.text("Start",0,0,32,null,"#ff00ff");
					isometrik.game.stop = true;
				}
				//
				// These two lines below should always be here,
				//    if not nothing will be displayed.
				isometrik.draw.composite();
				isometrik.draw.flip();
			} else {
				// null
			}
		},
		redraw: function ()
		{
			// You must store and recreate the screen image yourself,
			//	the engine will not do this for you.
		  	// Yet.
		},
		interaction: function(what, kind)
		{
			isometrik.interacting = true;
			if (kind == "keyboard") {
				debug(what);
				var key = {};
				key.code = (what.keyCode == 0) ? what.charCode : what.keyCode;
				key.char = String.fromCharCode(key.code).toLowerCase();
				//
				if (key.char == "c" && what.type == "keydown")
				{
					isometrik.clear("all", 0, 0, parseInt(isometrik.container.style.width,10), parseInt(isometrik.container.style.height,10));
				}

				if (key.char == "p" && what.type == "keydown" || key.code == 19 && what.type == "keydown")
				{
					if (isometrik.running == true)
					{
						isometrik.running = false;
					} else if (isometrik.running == false) {
						isometrik.running = true;
					}
				}

				if (key.char == "j" && what.type == "keydown" || key.code == 74 && what.type == "keydown")
				{
					isometrik.ajax.request("./js/level1.js", isometrik.loadLevelData);
				}
			};
			isometrik.interacting = false;
		}
	}
}());