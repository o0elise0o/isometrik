////
// Isometrik game engine code, written by Elise Nicole.
////

// Debugging Toast.show()/console.log() toggle variable.
//		true = debugging on
//		false = debugging off 
//
// To truely disable debugging output, like for a finished
//	product do the following:
//		1. Search(using a text editor) for all instances
//		of "debug(" and replace them with "//debug(".
//		2. Comment out the debug() function, declaration and body.
//		3. Comment out the debugging variable below.
//
// Once you run build.cmd all comments are removed, so all debugging
//	code will be removed. :-D
debugging = true;

// Used for printing messages as debugging information.
//	Replaces console.log()/Toast.show() for this purpose,
//	that way debugging can be toggled on or off using the
//	debugging variable located above.
function debug(what)
{
	if (debugging)
	{
		try {
			Toast.show(what);
		}
		catch (why) {
			try {
				console.log(what);
			}
			catch (why) {
				try {
					window.external.consoleLog(what);
				}
				catch (why) {
					//alert("There appears to be no Console to log to and Debugging output is turned on.");
				}
			}
		}
	} else {
		// Null
	}
};

// Impliment requestAnimationFrame and cancelAnimationFrame,
//	If the browser/container doesn't already support it.
(function() {
	debug("Ensuring window.requestAnimationFrame() exhists.")
    var lastTime = 0;
    var startTime = new Date().getTime();
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
    	debug("WARNING: window.requestAnimationFrame() still doesn't exhist, creating fallback using window.setTimeout()");
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime - startTime); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// Make sure the isometrik object exhists,
//   or if for some reason this file is loaded more then once,
//   completely erase the already created isometrik object.
var isometrik = {};

// State Machine Structure.
//	A basic state machine implimentation.
function StateMachine(states)
{
	this.states = states;
	this.indexes = {};
	for( var i = 0; i< this.states.length; i++){
		this.indexes[this.states[i].name] = i;
		if (this.states[i].initial){
			this.currentState = this.states[i];
		}
	}
	this.consumeEvent = function(e){
		if(this.currentState.events[e]){
			this.currentState = this.states[this.indexes[this.currentState.events[e]]] ;
		}
	}
	this.getStatus = function(){
		return this.currentState.name;
	}
};

// Linked List data type.
//	This is an implimentation of a singularly linked list structure,
//	it supports:
//		Add, remove, insert after, insert as first,
//		returning an item based on index,
//		circularly linking the list,
//		both timed and immediate execution of a list containing
//			functions as each nodes data.
//
//	WARNING:
//		Immediate execution of function lists, while supported,
//		is not recommended. Also, immediate or timed execution of
//		a circularly linked list is not recommended.
//		Browsers block during the function execution and lock up.
//		Even with timed execution a circularly linked list of functions
//		can lock up the browser, because it's an infinite blocking loop.
//		With a large enough wait time between function calls this can
//		probably be avoided.
function List() 
{
	this.start = null;
	this.end = null;
	
	this.makeNode = function() {
		return {
			data: null,
			next: null
		};
	}
	
	this.add = function(data) {
		if (this.start === null)
		{
			this.start = this.makeNode();
			this.end = this.start;
		} else {
			this.end.next = this.makeNode();
			this.end = this.end.next;
		};
	  this.end.data = data;
	 }
	 
	this.remove = function(data) {
		var current = this.start;
		var previous = this.start;
		while (current !== null)
		{
			if (data === current.data)
			{
				if (current === this.start) 
				{
					this.start = current.next;
					return;
				};
				if (current === this.end)
				{
					this.end = previous;
					previous.next = current.next;
					return;
				};
				previous = current;
				current = current.next;
			};
		};
	}
	
	this.insertAsFirst = function(d) {
		var temp = this.makeNode();
		temp.next = this.start;
		this.start = temp;
		temp.data = d;
	}
	
	this.insertAfter = function(t, d) {
		var current = this.start;
		while (current !== null)
		{
			if (current.data === t)
			{
				var temp = this.makeNode();
				temp.data = d;
				temp.next = current.next;
				if (current === this.end)
				{
					this.end = temp;
				}
				current.next = temp;
				return;
			}
			current = current.next;
		}
	}
	
	this.item = function(i) {
		var current = this.start;
		while (current !== null)
		{
			i--;
			if (i === 0)
			{
				return current;
			}
			current = current.next;
		}
		return null;
	}
	
	this.each = function(f) {
		var current = this.start;
		while (current !== null)
		{
			f(current);
			current = current.next;
		}
	}
	
	this.run = function() {
		this.each(function(what){what.data();});
	}
	
	this.close = function() {
		this.end.next = this.start;
	}
	
	this.timed = function(begin, pause) {
		begin = begin || 1;
		pause = pause || 1000;
		setTimeout(this.start.data, begin);
		var padTime = pause
		var current = this.start.next;
		while (current !== null)
		{
			setTimeout(current.data, padTime);
			current = current.next;
			padTime = padTime + pause;
		}
	}
};

// Pixel data type.
//	Used to store pixel color data.
//	Supports conversion of the pixel data to hex string,
//	or a CSS style rgba() string.
function Pixel(r,g,b,a)
{
	this.r = r || 0;
	this.g = g || 0;
	this.b = b || 0;
	this.a = a || 0;
	this.toHex = function () {
		return "#" + ((1 << 24) + (this.r << 16) + (this.g << 8) + this.b).toString(16).slice(1);
	}
	this.toRgbaString = function () {
		return "rgba(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")";
	}
};

// Tile data type.
function Tile(label, imaj, type, data, passable)
{
	this.label = label || "";
	this.imaj = imaj || {
		art: [],
		collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
	};
	this.type = type || {
		liquid: false,
		pain: false,
		wall: false,
		hole: false,
		exit: false
	};
	this.data = data || {
		damage: 0,
		target: [null, 0, 0]
	};
	this.passable = passable || {
		right: false,
		left: false,
		up: false,
		down: false,
		above: false,
		below: false
	};
};

// Sprite data type.
function Sprite(XYZ, DXYZ, ST, STS, DATA, LAYER, PRIORITY)
{
	this.X = XYZ[0] || 0;
	this.Y = XYZ[1] || 0;
	this.Z = XYZ[2] || 0;
	this.deltaX = DXYZ[0] || 0;
	this.deltaY = DXYZ[1] || 0;
	this.deltaZ = DXYZ[2] || 0;
	this.state = ST || 0;
	this.states = STS || {};
	this.data = DATA || 0;
	this.layer = LAYER || 3;
	this.priority = PRIORITY || 0;
};

isometrik.getMaxResolution = function() {
	var w = [];
	w[0] = screen.availWidth || 999999999;
	w[1] = window.innerWidth || 999999999;
	w[2] = document.body.clientWidth || 999999999;
	w.sort(function(a, b) {
    	return a - b;
	});

	var h = [];
	h[0] = screen.availHeight || 999999999;
	h[1] = window.innerHeight || 999999999;
	h[2] = document.body.clientHeight || 999999999;
	h.sort(function(a, b) {
    	return a - b;
	});
	debug("--- Resolution Width: " + w);
	debug("--- Resolution Height: " + h);
	//isometrik.width = w[0];
	//isometrik.height = h[0];
	return [w[0],h[0]];
};

// Build the entire API, and do first run stuff.
isometrik.init = function(fullscreen, tileSizeX, tileSizeY)
{
	debug("Entering isometrik.init().");
	//
	// Set the default tile size to 32x32 if not specified
	//	in the isometrik.init() function call.
	isometrik.tileSizeX = tileSizeX || 32;
	isometrik.tileSizeY = tileSizeY || 32;
	//
	// Set fullscreen to false if not specificed
	//	in the isometrik.init() function call.
	fullscreen = fullscreen || false;
	//
	// Grab the container object from the HTML host container.
	isometrik.container = document.getElementById("isometrik");
	//
	if (fullscreen)
	{
		// Fullscreen is requested, so...
		//
		// Figure out what the actual surface size is and
		//	fill in isometrik.width and isometrik.height
		//	with the determined size.
		var tmpRes = isometrik.getMaxResolution();
		//
		// Set the container objects width and height to the
		//	same size as the surface area.
		isometrik.width = tmpRes[0];
		isometrik.height = tmpRes[1];
		isometrik.container.style.width = tmpRes[0];
		isometrik.container.style.height = tmpRes[1];
	} else {
		// Fullscreen was not requested, so...
		//
		// Set the isometrik.width and isometrik.height to
		//	the width and height of the container object.
		isometrik.width = isometrik.container.style.width;
		isometrik.height = isometrik.container.style.height;
	};
	//

	// Create a bunch of HTML as a string to use with innerHTML.
	// I know, this is considered wrong, but I couldn't get all the browsers
	//	to agree to fucking work correctly if I used the DOM to start adding
	//	canvas objects, so I said fuck it I'll just use what actually works.
	//
	//	The canvases that are created are:
	//		The three screen buffers,
	//		The three User Interface layers,
	//		The five tile layers,
	//		One cache layer that never gets used for anything currently.
	var tmp = "";
	tmp += "<canvas id='buffer1' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "'></canvas>";
	tmp += "<canvas id='buffer2' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	tmp += "<canvas id='buffer3' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	//
	tmp += "<canvas id='ui1' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	tmp += "<canvas id='ui2' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	tmp += "<canvas id='ui3' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	//
	tmp += "<canvas id='underground' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	tmp += "<canvas id='ground' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	tmp += "<canvas id='wall' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	tmp += "<canvas id='roof' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	tmp += "<canvas id='sky' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	//
	tmp += "<canvas id='cache' width='" + isometrik.container.style.width + "'";
	tmp += " height='" + isometrik.container.style.height + "' style='display:none;'></canvas>";
	//
	// Insert that big string of HTML located above into
	//	the container object.
	isometrik.container.innerHTML = tmp;
	//
	isometrik.b1 = document.getElementById("buffer1");
	isometrik.b2 = document.getElementById("buffer2");
	isometrik.b3 = document.getElementById("buffer3");
	//
	isometrik.buffer1 = isometrik.b1.getContext("2d");
	isometrik.buffer2 = isometrik.b2.getContext("2d");
	isometrik.buffer3 = isometrik.b3.getContext("2d");
	//
	isometrik.buffer1.dirty = 0;
	isometrik.buffer2.dirty = 0;
	isometrik.buffer3.dirty = 0;
	//
	
	isometrik.p1 = document.getElementById("underground");
	isometrik.p2 = document.getElementById("ground");
	isometrik.p3 = document.getElementById("wall");
	isometrik.p4 = document.getElementById("roof");
	isometrik.p5 = document.getElementById("sky");
	//
	isometrik.plane1 = isometrik.p1.getContext("2d");
	isometrik.plane2 = isometrik.p2.getContext("2d");
	isometrik.plane3 = isometrik.p3.getContext("2d");
	isometrik.plane4 = isometrik.p4.getContext("2d");
	isometrik.plane5 = isometrik.p5.getContext("2d");
	//
	isometrik.plane1.dirty = 0;
	isometrik.plane2.dirty = 0;
	isometrik.plane3.dirty = 0;
	isometrik.plane4.dirty = 0;
	isometrik.plane5.dirty = 0;
	//

	isometrik.ui1 = document.getElementById("ui1");
	isometrik.ui2 = document.getElementById("ui2");
	isometrik.ui3 = document.getElementById("ui3");
	//
	isometrik.gui1 = isometrik.ui1.getContext("2d");
	isometrik.gui2 = isometrik.ui2.getContext("2d");
	isometrik.gui3 = isometrik.ui3.getContext("2d");
	//
	isometrik.gui1.dirty = 0;
	isometrik.gui2.dirty = 0;
	isometrik.gui3.dirty = 0;
	//

	isometrik.c1 = document.getElementById("cache");
	isometrik.cache = isometrik.c1.getContext("2d");
	isometrik.cache.dirty = 0;
	//

	//
	//GameController.init({canvas: isometrik.b1});
	//
	isometrik.chain = new List();
	isometrik.states = [];
	isometrik.loopVar = "";
	isometrik.gameVar = "";
	//isometrik.game = {};
	isometrik.loading = 0;
	//
	isometrik.data = {};
	isometrik.data.tiles = [];
	isometrik.data.maps = [];
	isometrik.data.sprites = [];
	isometrik.data.images = [];
	//
	if (window.location.search.length > 3)
	{
		isometrik.getQueryVariable = function(variable)
		{
		   var query = window.location.search.substring(1);
		   var vars = query.split("&");
		   for (var i=0;i<vars.length;i++) {
			   var pair = vars[i].split("=");
			   if(pair[0] == variable){return pair[1];}
		   }
		   return(false);
		}
	} else {
		isometrik.getQueryVariable = function(variable)
		{
			return(false);
		}
	};

	isometrik.clear = function (which, x, y, w, h) {
		x = x || 0;
		y = y || 0;
		w = w || 0;
		h = h || 0;
		which = which || "all"
		if (which == "all") {
			isometrik.buffer1.clearRect(x,y,w,h);
			isometrik.buffer2.clearRect(x,y,w,h);
			isometrik.buffer3.clearRect(x,y,w,h);
			//
			isometrik.plane1.clearRect(x,y,w,h);
			isometrik.plane2.clearRect(x,y,w,h);
			isometrik.plane3.clearRect(x,y,w,h);
			isometrik.plane4.clearRect(x,y,w,h);
			isometrik.plane5.clearRect(x,y,w,h);
			//
			isometrik.gui1.clearRect(x,y,w,h);
			isometrik.gui2.clearRect(x,y,w,h);
			isometrik.gui3.clearRect(x,y,w,h);
		} else if (which == "underground" || which == 0) {
			isometrik.plane1.clearRect(x,y,w,h);
		} else if (which == "ground" || which == 1) {
			isometrik.plane2.clearRect(x,y,w,h);
		} else if (which == "wall" || which == 2) {
			isometrik.plane3.clearRect(x,y,w,h);
		} else if (which == "roof" || which == 3) {
			isometrik.plane4.clearRect(x,y,w,h);
		} else if (which == "sky" || which == 4) {
			isometrik.plane5.clearRect(x,y,w,h);
		} else if (which == "buffer1") {
			isometrik.buffer1.clearRect(x,y,w,h);
		} else if (which == "buffer2") {
			isometrik.buffer2.clearRect(x,y,w,h);
		} else if (which == "buffer3") {
			isometrik.buffer3.clearRect(x,y,w,h);
		} else if (which == "gui") {
			isometrik.gui1.clearRect(x,y,w,h);
		} else if (which == "gui1") {
			isometrik.gui1.clearRect(x,y,w,h);
		} else if (which == "gui2") {
			isometrik.gui2.clearRect(x,y,w,h);
		} else if (which == "gui3") {
			isometrik.gui3.clearRect(x,y,w,h);
		} else {
			//
		};
	};

	isometrik.preloadImage = function(what) {
		isometrik.loading += 1;
		var tmp1 = new Image();
		tmp1.onload = function() {debug("Loaded image: " + what);isometrik.loading -= 1;};
		tmp1.src = what;
		return tmp1;
	};

	isometrik.convert = (function () {
		return {
			imageToArray: function (which) {
				var tmpCanvas = document.createElement('canvas');
				tmpCanvas.id = "tmpCanvas";
				tmpCanvas.width = which.imaj.art.width;
				tmpCanvas.height = which.imaj.art.height;
				var tmpContext = tmpCanvas.getContext("2d");
				tmpContext.drawImage(which.imaj.art,0,0);
				var tmpImageData = tmpContext.getImageData(0, 0, tmpCanvas.width, tmpCanvas.height);
				return tmpImageData.data;
			},
			arrayToImage: function () {

			},
			imageToPixelArray: function () {

			},
			pixelArrayToImage: function () {

			}
		};
	}());

	isometrik.draw = (function () {
		return {
			flip: function () {
			// Flip the composed image from buffer3 to the backbuffer
				if (isometrik.buffer3.dirty == 1) 
				{
					isometrik.buffer2.drawImage(isometrik.b3,0,0);
					isometrik.buffer2.dirty = 1;
					isometrik.buffer3.dirty = 0;
				} else {
					// Null
				}
			},
			composite: function () {
			// Compose all the seperate canvas surface images into one on buffer3
				if (isometrik.plane1.dirty == 1) {isometrik.buffer3.drawImage(isometrik.p1,0,0);isometrik.buffer3.dirty = 1;}
				if (isometrik.plane2.dirty == 1) {isometrik.buffer3.drawImage(isometrik.p2,0,0);isometrik.buffer3.dirty = 1;}
				if (isometrik.plane3.dirty == 1) {isometrik.buffer3.drawImage(isometrik.p3,0,0);isometrik.buffer3.dirty = 1;}
				if (isometrik.plane4.dirty == 1) {isometrik.buffer3.drawImage(isometrik.p4,0,0);isometrik.buffer3.dirty = 1;}
				if (isometrik.plane5.dirty == 1) {isometrik.buffer3.drawImage(isometrik.p5,0,0);isometrik.buffer3.dirty = 1;}
				//
				if (isometrik.gui1.dirty == 1) {isometrik.buffer3.drawImage(isometrik.ui1,0,0);isometrik.buffer3.dirty = 1;}
				if (isometrik.gui2.dirty == 1) {isometrik.buffer3.drawImage(isometrik.ui2,0,0);isometrik.buffer3.dirty = 1;}
				if (isometrik.gui3.dirty == 1) {isometrik.buffer3.drawImage(isometrik.ui3,0,0);isometrik.buffer3.dirty = 1;}
				//
				// isometrik.buffer3.dirty = 1;
				//
				isometrik.plane1.dirty = 0;
				isometrik.plane2.dirty = 0;
				isometrik.plane3.dirty = 0;
				isometrik.plane4.dirty = 0;
				isometrik.plane5.dirty = 0;
				//
				isometrik.gui1.dirty = 0;
				isometrik.gui2.dirty = 0;
				isometrik.gui3.dirty = 0;
			},
			text: function (what, x, y, size, w, color, onWhat) {
			// Draw configurable text
				size = size || 16;
				w = w || null;
				x = x || 0;
				y = y || 0;
				x = x + size;
				y = y + size;
				color = color || "white";
				onWhat = onWhat || "gui1";
				if (onWhat == "underground" || onWhat == 0) {onWhat = "plane1";};
				if (onWhat == "ground" || onWhat == 1) {onWhat = "plane2";};
				if (onWhat == "wall" || onWhat == 2) {onWhat = "plane3";};
				if (onWhat == "roof" || onWhat == 3) {onWhat = "plane4";};
				if (onWhat == "sky" || onWhat == 4) {onWhat = "plane5";};
				if (onWhat == "gui" || onWhat == 5) {onWhat = "gui1";};
				if (onWhat == "gui1" || onWhat == 5) {onWhat = "gui1";};
				if (onWhat == "gui2" || onWhat == 6) {onWhat = "gui2";};
				if (onWhat == "gui3" || onWhat == 7) {onWhat = "gui1";};
				//
				isometrik[onWhat].font = size + 'px Ariel';
				isometrik[onWhat].textAlign = 'center';
				isometrik[onWhat].fillStyle = color;
				if (w == null) 
				{
					isometrik[onWhat].fillText(what, x, y);
					isometrik[onWhat].dirty = 1;					
					
				} else {
					isometrik[onWhat].fillText(what, x, y, w);
					isometrik[onWhat].dirty = 1;
				}
			},
			image: (function () {
			// Functions to support drawing tiles from image files
				return {
					tile: function(what, where, onWhat) {
					// Draw a single rectangle shaped tile from an image file
						if (onWhat == "underground" || onWhat == 0) {onWhat = "plane1";};
						if (onWhat == "ground" || onWhat == 1) {onWhat = "plane2";};
						if (onWhat == "wall" || onWhat == 2) {onWhat = "plane3";};
						if (onWhat == "roof" || onWhat == 3) {onWhat = "plane4";};
						if (onWhat == "sky" || onWhat == 4) {onWhat = "plane5";};
						if (onWhat == "gui" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui1" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui2" || onWhat == 6) {onWhat = "gui2";};
						if (onWhat == "gui3" || onWhat == 7) {onWhat = "gui1";};
						//
						isometrik[onWhat].drawImage(isometrik.data.tiles[what].imaj.art, where[0], where[1]);
						isometrik[onWhat].dirty = 1;
					},
					map: function(which, where, onWhat) {
					// Draw a rectangle shaped tile area from image files
						if (onWhat == "underground" || onWhat == 0) {onWhat = "plane1";};
						if (onWhat == "ground" || onWhat == 1) {onWhat = "plane2";};
						if (onWhat == "wall" || onWhat == 2) {onWhat = "plane3";};
						if (onWhat == "roof" || onWhat == 3) {onWhat = "plane4";};
						if (onWhat == "sky" || onWhat == 4) {onWhat = "plane5";};
						if (onWhat == "gui" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui1" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui2" || onWhat == 6) {onWhat = "gui2";};
						if (onWhat == "gui3" || onWhat == 7) {onWhat = "gui1";};
						onWhat = onWhat || null;
						//		
						if (onWhat == null)
						{
							debug("Multi-layer map with no specific base layer detected.");
							// Draw a mapping with underground/layer 0 as the base.
							for(var a = 0;a <= which.length - 1;a++)
							{
								for(var b = 0;b <= which[a].length - 1;b++)
								{
									for(var c = 0;c <= which[a][b].length - 1;c++)
									{
										//isometrik.tileSizeX
										//isometrik.tileSizeY
										// a = z, b = y, c = x
										//
										var tempX = where[0] + (isometrik.tileSizeX * c);
										var tempY = where[1] + (isometrik.tileSizeY * b);
										var tempX2 = tempX / isometrik.tileSizeX;
										var tempY2 = tempY / isometrik.tileSizeY;
										isometrik.world.replace(isometrik.data.tiles[which[a][b][c]],[tempX2,tempY2,a]);
										isometrik.draw.image.tile(which[a][b][c],[tempX,tempY,0],a);
									}
								}
							}
						} else {
							// Handle mapping to specific layers here.
							if (which.length - 1 > 0)
							{
								// The map is multi-layer, so handle that here.
								debug("Multi-layer map with a specific base layer detected.");
							} else {
								// The map is single-layer, so handle that here.
							}
						}
					}
				};
			}()),
			flat: (function () {
			// Functions to support drawing rectangle tiles from pixel arrays
				return {
					tile: function(what, where, onWhat) {
					// Draw a single rectangle shaped tile from a pixel array
						if (onWhat == "underground" || onWhat == 0) {onWhat = "plane1";};
						if (onWhat == "ground" || onWhat == 1) {onWhat = "plane2";};
						if (onWhat == "wall" || onWhat == 2) {onWhat = "plane3";};
						if (onWhat == "roof" || onWhat == 3) {onWhat = "plane4";};
						if (onWhat == "sky" || onWhat == 4) {onWhat = "plane5";};
						if (onWhat == "gui" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui1" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui2" || onWhat == 6) {onWhat = "gui2";};
						if (onWhat == "gui3" || onWhat == 7) {onWhat = "gui1";};

						var len1 = what[0].length - 1;
						var len2 = what.length - 1;
						
						isometrik[onWhat].lineWidth = 1.0;
						for(var a=0;a<=len1;a++)
						{
							for(var b=0;b<=len2;b++)
							{
								if (what[a][b] == "-")
								{
									//.
								} else if (what[a][b] == "?") {
									var color = "";
									color = "#";
									color += Math.floor(Math.random()*16+0).toString(16);
									color += Math.floor(Math.random()*16+0).toString(16); 
									color += Math.floor(Math.random()*16+0).toString(16);
									color += Math.floor(Math.random()*16+0).toString(16);
									color += Math.floor(Math.random()*16+0).toString(16); 
									color += Math.floor(Math.random()*16+0).toString(16);
									isometrik[onWhat].fillStyle = color;
									isometrik[onWhat].fillRect(where[0] + b, where[1] + a, 1, 1);
								} else {
									isometrik[onWhat].fillStyle = what[a][b];
									isometrik[onWhat].fillRect(where[0] + b, where[1] + a, 1, 1);
								};
							};
						};
						isometrik[onWhat].fill();
						isometrik[onWhat].dirty = 1;
					},
					map: function(what, where, onWhat) {
					// Draw a rectangle shaped tile are from pixel arrays
						if (onWhat == "underground" || onWhat == 0) {onWhat = "plane1";};
						if (onWhat == "ground" || onWhat == 1) {onWhat = "plane2";};
						if (onWhat == "wall" || onWhat == 2) {onWhat = "plane3";};
						if (onWhat == "roof" || onWhat == 3) {onWhat = "plane4";};
						if (onWhat == "sky" || onWhat == 4) {onWhat = "plane5";};
						if (onWhat == "gui" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui1" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui2" || onWhat == 6) {onWhat = "gui2";};
						if (onWhat == "gui3" || onWhat == 7) {onWhat = "gui1";};

						var len1 = what[0].length - 1;
						var len2 = what.length - 1;
						
						for(var a=0;a<=len1;a++)
						{
							for(var b=0;b<=len2;b++)
							{
								if (what[b][a] == "-")
								{
									//.
								} else {
									isometrik.draw.flat.tile(isometrik.data.tiles[what[b][a]],[16 * a + where[0], 16 * b + where[1], 0 + where[2]], onWhat);
								};
							};
						}
						isometrik[onWhat].dirty = 1;
					}
				};
			}()),
			isometric: (function () {
			// Functions to support drawing isometric tiles from pixel arrays
				return {
					tile: function(what, where, onWhat) {
					// Draw a single diamond shaped tile from a pixel array
						if (onWhat == "underground" || onWhat == 0) {onWhat = "plane1";};
						if (onWhat == "ground" || onWhat == 1) {onWhat = "plane2";};
						if (onWhat == "wall" || onWhat == 2) {onWhat = "plane3";};
						if (onWhat == "roof" || onWhat == 3) {onWhat = "plane4";};
						if (onWhat == "sky" || onWhat == 4) {onWhat = "plane5";};
						if (onWhat == "gui" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui1" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui2" || onWhat == 6) {onWhat = "gui2";};
						if (onWhat == "gui3" || onWhat == 7) {onWhat = "gui1";};

						var height = what.length - 1;
						var width = what[0].length - 1;
						
						isometrik[onWhat].lineWidth = 1;
						for(var a=0;a<=height;a++)
						{
							width = what[a].length - 1;
							for(var b=0;b<=width;b++)
							{
								if (typeof what[a][b] == "object")
								{
									var color = ""
									color += "rgba(" + what[a][b].red;
									color += ", " + what[a][b].green;
									color += ", " + what[a][b].blue;
									color += ", " + what[a][b].alpha;
									color += ")";
									isometrik[onWhat].fillStyle = color;
									isometrik[onWhat].fillRect(where[0] + b - a, where[1] + a + b, 2, 2);
								} else {
									if (what[a][b] == "-")
									{
										// null
									} else if (what[a][b] == "?") {
										var color = "";
										color = "#";
										color += Math.floor(Math.random()*16+0).toString(16);
										color += Math.floor(Math.random()*16+0).toString(16); 
										color += Math.floor(Math.random()*16+0).toString(16);
										color += Math.floor(Math.random()*16+0).toString(16);
										color += Math.floor(Math.random()*16+0).toString(16); 
										color += Math.floor(Math.random()*16+0).toString(16);
										isometrik[onWhat].fillStyle = color;
										isometrik[onWhat].fillRect(where[0] + b - a, where[1] + a + b, 2, 2);
									} else if (isometrik.draw.blender.regEx.test(what[a][b])) {
										// Add Blender support here
										var first = what[a][b].charAt(0);
										var second = what[a][b].charAt(1);
										var color1 = "";
										var color2 = "";
										switch (first)
										{
											case "-":
												color1 = new Pixel(0,0,0,1.0);
											break;
											case "+":
												color1 = new Pixel(255,255,255,1.0);
											break;
											case "x":
												color1 = new Pixel(0,0,0,0.1);
											break;
											case "<":
												color1 = isometrik.getPixel(where[0] + b - a - 1, where[1] + a + b - 1, "buffer");
											break;
											case "^":
												color1 = isometrik.getPixel(where[0] + b - a + 1, where[1] + a + b - 1, "buffer");
											break;
											case ">":
												color1 = isometrik.getPixel(where[0] + b - a + 1, where[1] + a + b + 1, "buffer");
											break;
											case "v":
											break;
										};
										switch (second)
										{
											case "-":
												color2 = new Pixel();
											break;
											case "+":
												color2 = new Pixel(255,255,255,1.0);
											break;
											case "x":
												color2 = new Pixel(color1.r,color1.g,color1.b,0.0);
											break;
											case "2":
												color2 = new Pixel(color1.r/2,color1.g/2,color1.b/2,color1.a+0.1);
											break;
											case "<":
												color2 = isometrik.getPixel(where[0] + b - a - 1, where[1] + a + b - 1, "buffer");
											break;
											case "^":
												color2 = isometrik.getPixel(where[0] + b - a + 1, where[1] + a + b - 1, "buffer");
											break;
											case ">":
												color2 = isometrik.getPixel(where[0] + b - a + 1, where[1] + a + b + 1, "buffer");
											break;
											case "v":
											break;
										};
										////console.log(isometrik.draw.blender.computeColor(color1, color2).toRgbaString());
										isometrik[onWhat].fillStyle = isometrik.draw.blender.computeColor(color1, color2).toRgbaString();
										isometrik[onWhat].fillRect(where[0] + b - a, where[1] + a + b, 2, 2);
									} else {
										isometrik[onWhat].fillStyle = what[a][b];
										//var tmpWhere1 =  [where[0] + b, where[1] + a, where[2]];
										//var tmpWhere2 = isometrik.matrix.multiply1x3(tmpWhere1, [[1,0,0 - a],[0,1, b],[0,0,1]]);
										//console.log(tmpWhere2);
										isometrik[onWhat].fillRect(where[0] + b - a, where[1] + a + b, 2, 2);
										//isometrik[onWhat].fillStyle = "#FF00FF";
										//isometrik[onWhat].fillRect(tmpWhere2[0], tmpWhere2[1], 2, 2);
									};
								};
							};
						};
						isometrik[onWhat].fill();
						isometrik[onWhat].dirty = 1;
					},
					map: function(what, where, onWhat) {
					// Draw an isometric diamond shaped tile area from pixel arrays
						if (onWhat == "underground" || onWhat == 0) {onWhat = "plane1";};
						if (onWhat == "ground" || onWhat == 1) {onWhat = "plane2";};
						if (onWhat == "wall" || onWhat == 2) {onWhat = "plane3";};
						if (onWhat == "roof" || onWhat == 3) {onWhat = "plane4";};
						if (onWhat == "sky" || onWhat == 4) {onWhat = "plane5";};
						if (onWhat == "gui" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui1" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui2" || onWhat == 6) {onWhat = "gui2";};
						if (onWhat == "gui3" || onWhat == 7) {onWhat = "gui1";};

						var len1 = what[0].length - 1;
						var len2 = what.length - 1;

						for(var a=0;a<=len1;a++)
						{
							for(b=0;b<=len2;b++)
							{
								if (what[b][a] == "-")
								{
									//.
								} else {
									var tmpWhere = [
										where[0] + (isometrik.tileSizeX * a) - (isometrik.tileSizeX * b),
										where[1] + (isometrik.tileSizeX * b) + (isometrik.tileSizeX * a),
										0
									];
									isometrik.draw.isometric.tile(isometrik.data.tiles[what[b][a]], tmpWhere, onWhat);
								}
							}
						}
						isometrik[onWhat].dirty = 1;
					},
					squareMap: function(what, where, onWhat) {
					// Draw an isometric 'square' shaped tile area from pixel arrays
						if (onWhat == "underground" || onWhat == 0) {onWhat = "plane1";};
						if (onWhat == "ground" || onWhat == 1) {onWhat = "plane2";};
						if (onWhat == "wall" || onWhat == 2) {onWhat = "plane3";};
						if (onWhat == "roof" || onWhat == 3) {onWhat = "plane4";};
						if (onWhat == "sky" || onWhat == 4) {onWhat = "plane5";};
						if (onWhat == "gui" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui1" || onWhat == 5) {onWhat = "gui1";};
						if (onWhat == "gui2" || onWhat == 6) {onWhat = "gui2";};
						if (onWhat == "gui3" || onWhat == 7) {onWhat = "gui1";};

						var len1 = what.length - 1;
						// var len2 = what[0].length - 1;

						for(var a=0;a<=len1;a++) //len1
						{
							var len2 = what[a].length - 1;
							for(b=0;b<=len2;b++) //len2
							{
								if (what[a][b] == "-")
								{
									//.
								} else {
									if (a % 2 !== 0)
									{
										console.log("Odd: " + a);
										var tmpWhere = [
											where[0] + (isometrik.tileSizeX * b) - (isometrik.tileSizeX * a) + (isometrik.tileSizeX * b) + (isometrik.tileSizeX * a) + isometrik.tileSizeX,
											where[1] + (isometrik.tileSizeX * a) + (isometrik.tileSizeX * b) - (isometrik.tileSizeY * b),
											0
										];
									} else {
										console.log("Even: " + a);
										var tmpWhere = [
											where[0] + (isometrik.tileSizeX * b) - (isometrik.tileSizeX * a) + (isometrik.tileSizeX * b) + (isometrik.tileSizeX * a),
											where[1] + (isometrik.tileSizeX * a) + (isometrik.tileSizeX * b) - (isometrik.tileSizeY * b),
											0
										];
									}
									// var tmpWhere = [
									// 	where[0] + (isometrik.tileSizeX * a) - (isometrik.tileSizeX * b) + (isometrik.tileSizeX * a),
									// 	where[1] + (isometrik.tileSizeX * b) + (isometrik.tileSizeX * a) - (isometrik.tileSizeY * a),
									// 	0
									// ];
									// var tmpWhere = [
									// 	where[0] + (isometrik.tileSizeX * b) - (isometrik.tileSizeX * a) + (isometrik.tileSizeX * b),
									// 	where[1] + (isometrik.tileSizeX * a) + (isometrik.tileSizeX * b) - (isometrik.tileSizeY * b),
									// 	0
									// ];
									isometrik.draw.isometric.tile(isometrik.data.tiles[what[a][b]], tmpWhere, onWhat);
									//isometrik.draw.isometric.tile(isometrik.create.solid("?",32,32), tmpWhere, onWhat);
								}
							}
						}
						isometrik[onWhat].dirty = 1;
					}					
				};
			}()),
			blender: (function () {
			// Functions to support per-pixel blending features
				return {
					regEx: /[2vV<>^xX\-\+][2vV<>^xX\-\+]/,
					rgbToHex: function (r, g, b) {
					// Convert seperate values of red, green, and blue into a hex string
						return "" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
					},
					hexToRgb: function (hex) {
					// Convert a hex string into a Pixel data type
						// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
						var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
						hex = hex.replace(shorthandRegex, function(m, r, g, b) {return r + r + g + g + b + b;});

						var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
						return result ? {
							r: parseInt(result[1], 16),
							g: parseInt(result[2], 16),
							b: parseInt(result[3], 16),
							a: 1.0
						} : new Pixel();
					},
					computeColor: function (color1, color2) {
					// Compute a color in between two colors
						var c1 = (typeof color1 == 'Pixel') ? this.hexToRgb(color1) : color1;
						var c2 = (typeof color2 == 'Pixel') ? this.hexToRgb(color2) : color2;
						
						var c3r1 = Math.round(c1.r + c2.r / 2);
						var c3g1 = Math.round(c1.g + c2.g / 2);
						var c3b1 = Math.round(c1.b + c2.b / 2);
						var c3a1 = c1.a;
						
						var c3r2 = Math.round(c2.r + c1.r / 2);
						var c3g2 = Math.round(c2.g + c1.g / 2);
						var c3b2 = Math.round(c2.b + c1.b / 2);
						var c3a2 = c2.a;
						
						var c3r = c3r1 < c3r2 ? c3r1 : c3r2;
						var c3g = c3g1 < c3g2 ? c3g1 : c3g2;
						var c3b = c3b1 < c3b2 ? c3b1 : c3b2;
						var c3a = c1.a - c2.a / (1 + c1.a)//c3a1 > c3a2 ? c3a1 : c3a2;
						
						c3r = c3r >= 255 ? 255 : c3r;
						c3g = c3g >= 255 ? 255 : c3g;
						c3b = c3b >= 255 ? 255 : c3b;
						c3a = c3a >= 1.0 ? 1.0 : c3a;
						c3a = c3a <= 0.0 ? 0.0 : c3a;

						var c3 = new Pixel(c3r, c3g, c3b, c3a);//this.rgbToHex(c3r, c3g, c3b);

						return c3;
					},
					computeColors: function (color1, color2, length) {
					// Compute a configurable amount of colors in between two colors
					}
				};
			}())
		};
	}());
	
	isometrik.create = (function () {
	// Functions to support dynamically created pixel array images
		return {
			solid: function(color, width, height) {
			// Create a configurable solid colored pixel array
				var tmp = [];
				if (typeof color == "object")
						{
							var tmpc = ""
							tmpc += "rgba(" + color.red;
							tmpc += ", " + color.green;
							tmpc += ", " + color.blue;
							tmpc += ", " + color.alpha;
							tmpc += ")";
							color = tmpc;
				} else {
					if(color == "?")
					{
						color = "#";
						color += Math.floor(Math.random()*16+0).toString(16);
						color += Math.floor(Math.random()*16+0).toString(16); 
						color += Math.floor(Math.random()*16+0).toString(16);
						color += Math.floor(Math.random()*16+0).toString(16);
						color += Math.floor(Math.random()*16+0).toString(16); 
						color += Math.floor(Math.random()*16+0).toString(16);
					}
				}
				for(var z=0;z<=height - 1;z++)
				{
					tmp.push([]);
				}
				for(var a=0;a<=height - 1;a++)
				{
					for(var b=0;b<=width - 1;b++)
					{
						tmp[a].push(color);
					}
				}
				return tmp;
			},
			fromTemplate: function(template) {
				
			}
			// create.random.solid()
			// create.random.blend()
		};
	}());
	
	isometrik.getPixel = function(x, y, which) {
	// Get the pixel data from a configurable surface and return it as a Pixel data type
	// TODO: Why is this not in the Blender section?
		if (which == "underground" || which == 0) {which = "plane1";};
		if (which == "ground" || which == 1) {which = "plane2";};
		if (which == "wall" || which == 2) {which = "plane3";};
		if (which == "roof" || which == 3) {which = "plane4";};
		if (which == "sky" || which == 4) {which = "plane5";};
		if (onWhat == "gui" || onWhat == 5) {onWhat = "gui1";};
		if (onWhat == "gui1" || onWhat == 5) {onWhat = "gui1";};
		if (onWhat == "gui2" || onWhat == 6) {onWhat = "gui2";};
		if (onWhat == "gui3" || onWhat == 7) {onWhat = "gui1";};

		var tmp = isometrik[which].getImageData(x,y,1,1);
		tmp.data[3] = tmp.data[3] >= 1.0 ? tmp.data[3] / 2.55 / 100 : tmp.data[3];
		return new Pixel(tmp.data[0],tmp.data[1],tmp.data[2],tmp.data[3]);
	};
	
	isometrik.input = (function () {
	// Functions to support user interaction
		return {
			keyboard: function (e) {
			// Handle keyboard events
				e.preventDefault();
				isometrik.game.interaction(e, "keyboard");
			},
			mouse: function (e) {
			// Handle mouse events
				e.preventDefault();
				isometrik.game.interaction(e, "mouse");
			},
			touch: function (e) {
			// Handle touch events
				e.preventDefault();
				isometrik.game.interaction(e, "touch");
			}
		};
	}());
	
	isometrik.setResolution = function(newResolution) {
	// Set the resolution of the container object
		newResolution = (typeof newResolution == 'undefined') ? [isometrik.width, isometrik.height] : newResolution;
		isometrik.container.style.width = newResolution[0];
		isometrik.container.style.height = newResolution[1];
	};
	
	isometrik.resized = function() {
	// Call the game specific redraw() function when a resize is detected
		debug("Entering isometrik.resized().");
		//
		isometrik.game.redraw();
		//
		debug("Exiting isometrik.resized().");
	};
	
	isometrik.startLoop = function(tickRate, begin) {
	// 
		debug("Starting game loop.");
		begin = begin || false;
		tickRate = (typeof tickRate == 'undefined') ? 50 : tickRate;// tickRate = tickRate || 50;
		isometrik.loopVar = requestAnimationFrame(isometrik.flush);
		//isometrik.loopVar = setInterval(isometrik.flush, tickRate);
		if (begin == true)
		{
			isometrik.running = true;
			isometrik.gameVar = setInterval(isometrik.game.loop, tickRate);
		} else {
			isometrik.running = false;
			isometrik.gameVar = setInterval(isometrik.game.loop, tickRate);
		}
	};

	isometrik.flush = function() {
		if (isometrik.buffer2.dirty == 1)
		{
			isometrik.buffer1.drawImage(isometrik.b2,0,0);
			isometrik.buffer2.dirty = 0;
		} else {
			// Null
		}
		isometrik.loopVar = requestAnimationFrame(isometrik.flush);
	};

	isometrik.ajax = (function() {
		return {
			request: function(url, callback, what) {
				debug("Attempting AJAX request for: " + url);
				var httpRequest;
				what = what || null;
				if (window.XMLHttpRequest) {
					httpRequest = new XMLHttpRequest();
				} else if (window.ActiveXObject) {
					try {
						httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
					}
					catch (e) {
						try {
							httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
						} 
						catch (e) {
							//
						}
					}
				};

				if (!httpRequest) 
				{
					debug("Unable to create an XMLHTTP instance.");
					return false;
				};

				httpRequest.onreadystatechange = function () {
					try {
						if (httpRequest.readyState === 4) 
						{
							if (httpRequest.status === 200) 
							{
								callback(httpRequest.responseText);
							} else {
								debug("There was a problem with the AJAX request.");
							}
						}
					}
					catch(e) {
						debug("Caught Exception: " + e.description);
					}	
				};
				try {
					httpRequest.open('GET', url);
					httpRequest.send(what);
				}
				catch(e) {
					debug("Caught Exception: " + e.description);
				}
			}
		}
	}());

	isometrik.loadLevelData = function(what)
	{
		var tmp = eval(what);
		isometrik.game[tmp.name] = tmp;
		debug("loadLevelData(" + tmp.name + "): " + tmp);
	};

	isometrik.world = (function () {
		return {
			width: "",
			height: "",
			tilesWide: "",
			tilesHigh: "",
			map: [[],[],[],[],[]],
			initialize: function(width, height) {
				isometrik.world.width = width || 64;
				isometrik.world.height = height || 64;
				debug("Viewport Initialized to: " + isometrik.width + "x" + isometrik.height);				
				isometrik.world.tilesWide = Math.ceil(isometrik.width / isometrik.tileSizeX);
				isometrik.world.tilesHigh = Math.ceil(isometrik.height / isometrik.tileSizeY);
				debug("Viable tiles: " + isometrik.world.tilesWide + "x" + isometrik.world.tilesHigh);

				for (var c=0;c <= 4;c++)
				{
					for (var b=0;b <= isometrik.world.height - 1;b++)
					{
						isometrik.world.map[c][b] = [];
						for (var a=0;a <= isometrik.world.width - 1;a++)
						{
							isometrik.world.map[c][b][a] = new Tile();
						}
					}
				}
				debug("World Initialized to: " + isometrik.world.width + "x" + isometrik.world.height + "(" + (isometrik.world.width * isometrik.tileSizeX) + "x" + (isometrik.world.height * isometrik.tileSizeY) + ")");
			},
			add: function(what, where) {
				var tmp1 = isometrik.world.map[where[2]][where[1]][where[0]];
				// Label
				isometrik.world.map[where[2]][where[1]][where[0]].label = what.label;
				// Liquid
				if (tmp1.type.liquid == true || what.type.liquid == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].type.liquid = true;
				}
				// Pain
				if (tmp1.type.pain == true || what.type.pain == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].type.pain = true;
				}
				// Wall
				if (tmp1.type.wall == true || what.type.wall == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].type.wall = true;
				}
				// Hole
				if (tmp1.type.hole == true || what.type.hole == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].type.hole = true;
				}	
				// Exit
				if (tmp1.type.exit == true || what.type.exit == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].type.exit = true;
				}
				// Damage
				isometrik.world.map[where[2]][where[1]][where[0]].data.damage = tmp1.data.damage + what.data.damage;
				// Right
				if (tmp1.passable.right == true || what.passable.right == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].passable.right = true;
				}
				// Left
				if (tmp1.passable.left == true || what.passable.left == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].passable.left = true;
				}
				// Up
				if (tmp1.passable.up == true || what.passable.up == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].passable.up = true;
				}
				// Down
				if (tmp1.passable.down == true || what.passable.down == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].passable.down = true;
				}
				// Above
				if (tmp1.passable.above == true || what.passable.above == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].passable.above = true;
				}
				// Below
				if (tmp1.passable.below == true || what.passable.below == true)
				{
					isometrik.world.map[where[2]][where[1]][where[0]].passable.below = true;
				}
			},
			replace: function(what, where){
				isometrik.world.map[where[2]][where[1]][where[0]] = what;
			}
		}
	}());

	// Wait for art assets to load before entering isometrik.main().
	isometrik.tryBegin = function() 
	{
		if (isometrik.loading > 0)
		{
			debug("Art assests still loading, defering isometrik.main().")
			setTimeout(isometrik.tryBegin, 50);
		} else {
			//isometrik.hideLoadingAnimation();
			isometrik.main();
		}
	};

	//
	isometrik.showLoadingAnimation = function()
	{
		var tmp = document.getElementById("loading");
		tmp.style.display="block";
	};

	//
	isometrik.hideLoadingAnimation = function()
	{
		var tmp = document.getElementById("loading");
		tmp.style.display="none";
	}

	//
	isometrik.main = function()
	{
		debug("Entering isometrik.main().");
		//
		isometrik.game.main();
		//
		debug("Exiting isometrik.main().");
	};

	isometrik.matrix = (function () {
		var test1 = [
			[1,0,0],
			[0,1,0],
			[0,0,1]
		];

		var test2 = [
			[1,0,0],
			[0,1,0],
			[0,0,1]
		];
		return {
			multiply3x3: function(m1, m2)
			{
				//console.log(m1[2][0] + " * " + m2[0][2] + " + " + m1[2][1] + " * " + m2[1][2] + " + " + m1[2][2] + " * " + m2[2][2]);
				return [
					[
						(m1[0][0] * m2[0][0]) + (m1[0][1] * m2[1][0]) + (m1[0][2] * m2[2][0]),
						(m1[0][0] * m2[0][1]) + (m1[0][1] * m2[1][1]) + (m1[0][2] * m2[2][1]),
						(m1[0][0] * m2[0][2]) + (m1[0][1] * m2[1][2]) + (m1[0][2] * m2[2][2])
					],
					[
						(m1[1][0] * m2[0][0]) + (m1[1][1] * m2[1][0]) + (m1[1][2] * m2[2][0]),
						(m1[1][0] * m2[0][1]) + (m1[1][1] * m2[1][1]) + (m1[1][2] * m2[2][1]),
						(m1[1][0] * m2[0][2]) + (m1[1][1] * m2[1][2]) + (m1[1][2] * m2[2][2])
	 				],
					[
						(m1[2][0] * m2[0][0]) + (m1[2][1] * m2[1][0]) + (m1[2][2] * m2[2][0]),
						(m1[2][0] * m2[0][1]) + (m1[2][1] * m2[1][1]) + (m1[2][2] * m2[2][1]),
						(m1[2][0] * m2[0][2]) + (m1[2][1] * m2[1][2]) + (m1[2][2] * m2[2][2])
					]
				]
			},
			multiply1x3: function(v1, m1)
			{
				//console.log(test1);-
				return [
					(v1[0] * m1[0][0]) + (v1[1] * m1[0][1]) + (v1[2] * m1[0][2]),
					(v1[0] * m1[1][0]) + (v1[1] * m1[1][1]) + (v1[2] * m1[1][2]),
					(v1[0] * m1[2][0]) + (v1[1] * m1[2][1]) + (v1[2] * m1[2][2])
				]
			}
		}
	}());
	//
	// Register Events
	document.body.addEventListener("keydown", isometrik.input.keyboard, true);
	document.body.addEventListener("keyup", isometrik.input.keyboard, true);
	document.body.addEventListener("keypress", isometrik.input.keyboard, true);
	//
	document.body.addEventListener("mousedown", isometrik.input.mouse, true);
	document.body.addEventListener("mouseup", isometrik.input.mouse, true);
	document.body.addEventListener("mousemove", isometrik.input.mouse, true);
	//
	document.body.addEventListener("touchstart", isometrik.input.touch, true);
	document.body.addEventListener("touchend", isometrik.input.touch, true);
	document.body.addEventListener("touchcancel", isometrik.input.touch, true);
	document.body.addEventListener("touchleave", isometrik.input.touch, true);
	document.body.addEventListener("touchmove", isometrik.input.touch, true);

	window.addEventListener("resize", isometrik.resized, true);
	//
	////
	isometrik.loadData();
	isometrik.tryBegin();
	debug("Exiting isometrik.init().");
};