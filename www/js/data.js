isometrik.loadData = function()
{
	debug("Entering isometrik.loadData().")
	isometrik.data.tiles[0] = new Tile(
		"Blank",
		{
			art: isometrik.preloadImage("img/blank.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[1] = new Tile(
		"top-left",
		{
			art: isometrik.preloadImage("img/dungeon/top-left.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: true,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[2] = new Tile(
		"top-middle-001",
		{
			art: isometrik.preloadImage("img/dungeon/top-middle-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: true,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[3] = new Tile(
		"top-right",
		{
			art: isometrik.preloadImage("img/dungeon/top-right.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: true,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[4] = new Tile(
		"middle-left",
		{
			art: isometrik.preloadImage("img/dungeon/middle-left.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: true,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[5] = new Tile(
		"wall-top-middle-001",
		{
			art: isometrik.preloadImage("img/dungeon/wall-top-middle-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: true,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[6] = new Tile(
		"middle-right",
		{
			art: isometrik.preloadImage("img/dungeon/middle-right.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: true,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[7] = new Tile(
		"floor-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[8] = new Tile(
		"bottom-left",
		{
			art: isometrik.preloadImage("img/dungeon/bottom-left.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: true,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[9] = new Tile(
		"bottom-middle-001",
		{
			art: isometrik.preloadImage("img/dungeon/bottom-middle-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: true,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[10] = new Tile(
		"bottom-right",
		{
			art: isometrik.preloadImage("img/dungeon/bottom-right.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: true,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[11] = new Tile(
		"floor-top-left-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-top-left-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[12] = new Tile(
		"floor-top-middle-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-top-middle-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[13] = new Tile(
		"floor-top-right-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-top-right-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[14] = new Tile(
		"floor-middle-left-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-middle-left-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[15] = new Tile(
		"floor-water-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-water-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[16] = new Tile(
		"floor-middle-right-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-middle-right-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[17] = new Tile(
		"floor-bottom-left-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-bottom-left-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[18] = new Tile(
		"floor-bottom-middle-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-bottom-middle-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	isometrik.data.tiles[19] = new Tile(
		"floor-bottom-right-001",
		{
			art: isometrik.preloadImage("img/dungeon/floor-bottom-right-001.png"),
			collide: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
		},
		{
			liquid: false,
			pain: false,
			wall: false,
			hole: false,
			exit: false
		},
		{
			damage: 0,
			target: [null, 0, 0]
		},
		{
			right: false,
			left: false,
			up: false,
			down: false,
			above: false,
			below: false
		}
	);
	
	isometrik.data.tiles[255] = isometrik.create.solid("?",32,32);
	isometrik.data.tiles[254] = isometrik.create.solid("#FF0000",32,32);
	isometrik.data.tiles[253] = isometrik.create.solid("#00FF00",32,32);
	isometrik.data.tiles[252] = isometrik.create.solid("#0000FF",32,32);
	isometrik.data.tiles[251] = isometrik.create.solid("#FFFFFF",32,32);
	isometrik.data.testIsoMap = [
		[254,255,255,253],
		[255,255,255],
		[255,255,255,255],
		[255,255,255],
		[251,255,255,252]
	];
	//
	isometrik.data.maps[0] = [
			[
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			],
			[
				[7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7],
				[7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7],
				[7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7],
				[7,7,11,12,12,12,12,12,12,12,12,12,12,12,12,13,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,14,15,15,15,15,15,15,15,15,15,15,15,15,16,7,7],
				[7,7,17,18,18,18,18,18,18,18,18,18,18,18,18,19,7,7],
				[7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7],
				[7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7]
			],
			[
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			],
			[
				[1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
				[8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,10]
			],
			[
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			],
		];
	debug("Exiting isometrik.loadData().")
};